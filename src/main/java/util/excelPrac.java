package util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class excelPrac
{

	public static Object[][] readExcel() throws IOException 
	
	{
	
		//open the excel
		XSSFWorkbook wb=new XSSFWorkbook("./data/excelprac.xlsx");
		//go to the sheet
		XSSFSheet sheet = wb.getSheetAt(0);
		//to access entire row count(it will give entire record)
		int rowCount = sheet.getLastRowNum();
		//to access column count(it will give entire record)
		short cellCount = sheet.getRow(0).getLastCellNum();
		Object[][] data=new Object[rowCount][cellCount];
		//go to specific row
		//suppose we need all the info about rows and columns need to use 2 for loops
		for(int j=1;j<=rowCount;j++)
		{
		XSSFRow row = sheet.getRow(j);
		//go to specific cell
		//suppose we need to te specific row use for loop
		for(int i=0;i<cellCount;i++)
		{
			XSSFCell cell = row.getCell(i);
			//read the content(string)
			try
			{
			String value = cell.getStringCellValue();
			System.out.println(value);
			data[j-1][i]=value;
		}
			catch(NullPointerException e)
			{
				System.out.println(" ");
			}
		
		
		//close the excel
		wb.close();
		
	}
	}
		return data;
	}
}
