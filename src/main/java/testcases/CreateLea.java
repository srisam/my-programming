package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import util.excelPrac;
import wdMethods.ProjectMethods;

public class CreateLea extends ProjectMethods{

	@Test(groups="smoke" , dataProvider="getdata")
	public void createLead(String cmp, String fname, String lname)
	{
		
		
		WebElement elemen = locateElement("xpath", "//a[text()='Create Lead']");
		click(elemen);
		WebElement elemen1 = locateElement("id", "createLeadForm_companyName");
		type(elemen1,cmp );
		WebElement firstNm = locateElement("id", "createLeadForm_firstName");
		type(firstNm,fname);
		WebElement lastNm = locateElement("id", "createLeadForm_lastName");
		type(lastNm,lname);
		WebElement eleme2 = locateElement("class", "smallSubmit");
		click(eleme2);
		closeBrowser();
		
		
	}
	@DataProvider(name="getdata")
	public Object[][] fetchdata() throws IOException
	{
	Object[][] data = excelPrac.readExcel();
		return data;
		/*String[][] data=new String[2][3];
		data[0][0]="HCL";
		data[0][1]="Srivatsan";
		data[0][2]="AR";
		
		data[1][0]="Cognizant";
		data[1][1]="Sri";
		data[1][2]="AR";
	
		return data;*/
	}
}
