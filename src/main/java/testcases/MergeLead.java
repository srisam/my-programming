package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{

	@Test(groups="regression", dependsOnGroups="sanity")
	//(dependsOnMethods={"testcases.CreateLea.createLead"}, alwaysRun=true)
	public void mergeLead() throws InterruptedException
	{
		
		
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
		WebElement firstNm = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(firstNm,"Srivatsan");
		WebElement eleFindButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindButton);
		Thread.sleep(2000);
		WebElement eleFirstResultLead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFirstResultLead);
		WebElement eleEdit = locateElement("link", "Edit");
		click(eleEdit);
		WebElement eleCname = locateElement("id" , "updateLeadForm_companyName");
		eleCname.clear();
		type(eleCname, "TCS");
		WebElement eleUpdate = locateElement("class", "smallSubmit");
		click(eleUpdate);
		closeBrowser();
		
		
	}
}
