package practical;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods
{
@Test
	public void login() throws InterruptedException 
	{
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement eleSearch = locateElement("xpath", "//a[@title='Start your wonderful journey']");
		click(eleSearch);
	   WebElement eleStr = locateElement("xpath","//div[@class='component-popular-locations']/div[2]");
	   click(eleStr);
	   WebElement proceed = locateElement("class", "proceed");
	   click(proceed);
	   //WebElement eleXp = locateElement("xpath","//div[@class='component-date']/div[2]/div[2]");
	   
	   //Get current date
	   Date date=new Date();
	   
	   //get only the date (not a month,year,time etc)
	   DateFormat sdf=new SimpleDateFormat("dd");
	   
	   //Get today's date
	   String today=sdf.format(date);
	   
	   
	   //convert to integer and add 1 to it
	   
	   int tomorrow=Integer.parseUnsignedInt(today)+1;
	   
	   //print tomorrow's date
	   
	   System.out.println(tomorrow);
	
	   //using webelement getting next date 
	   String text = locateElement("xpath","//div[contains(text(),'"+tomorrow+"')]").getText();
	   
	   WebElement seleDat = locateElement("xpath","//div[contains(text(),'"+tomorrow+"')]");
	   click(seleDat);
	   WebElement eleVerify = locateElement("xpath", "//div[@class='day picked ']");
	   verifyPartialText(eleVerify, text);
	   //clicking next
	   WebElement clickNex = locateElement("xpath","//button[contains(text(),'Next')]");
	   
	   click(clickNex);
	   //clicking done
	   WebElement clickDon = locateElement("xpath","//button[contains(text(),'Done')]");
	   
	   click(clickDon);
	   Thread.sleep(3000);
	   List<WebElement> resultDisp = driver.findElementsByXPath("//div[@class='car-list-layout']");
	   
	   int size=resultDisp.size();
	   System.out.println(size);  
	   
	   List<WebElement> cll = driver.findElementsByXPath("//div[@class='price']");
	   List<Integer> lst = new ArrayList<>();
	   for (WebElement each : cll) {
		   String text2 = each.getText();
		   String replaceAll = text2.replaceAll("\\D", "");
           int parseInt = Integer.parseInt(replaceAll);
           lst.add(parseInt);
	}
	   System.out.println(lst);
	   Integer max = Collections.max(lst);
	   System.out.println(max);
	   
}
}










