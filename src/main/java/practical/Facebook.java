package practical;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class Facebook extends SeMethods

{
	@Test

	public void login() throws InterruptedException 
	{
		startApp("chrome", "https://www.facebook.com/");
		WebElement eleUsername = locateElement("id", "email");
		eleUsername.clear();
		eleUsername=locateElement("id","email");
		type(eleUsername,"srvatsan2@gmail.com");
		WebElement elePassword = locateElement("id","pass");
		elePassword.clear();
		elePassword=locateElement("id","pass");
		type(elePassword,"1stangelsam");
		WebElement sub = locateElement("xpath","//input[@data-testid='royal_login_button']");
		click(sub);
		WebElement search = locateElement("xpath","//input[@data-testid='search_input']");
		type(search,"testleaf");
		click(search);
		WebElement go = locateElement("xpath","//button[@data-testid='facebar_search_button']");
		click(go);
		WebElement eleLike = locateElement("xpath","(//button[@type='submit'])[2]");
		String text = locateElement("xpath","(//button[@type='submit'])[2]").getText();
		if(text.contains("Like"))
		{
			click(eleLike);
			System.out.println("the like button is clicked");
		}else {
			System.out.println("the like button is already clicked");
		}


		WebElement clic = locateElement("xpath","//div[text()='TestLeaf']");
		click(clic);
		Thread.sleep(2000);
		String count = locateElement("xpath", "//div[contains(text(),'people like this')]").getText();
		String replaceAll = count.replaceAll("\\D", "");
		System.out.println(replaceAll);
	}

	}