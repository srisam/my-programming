package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElements {

	public static void main(String[] args) throws InterruptedException 
	{
		//invoking the chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//maximize the output window
		driver.manage().window().maximize();
		//load the url using get() method
		driver.get("https://erail.in");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SRGM",Keys.TAB);
		Thread.sleep(3000);
		//Table
		WebElement table = driver.findElementByXPath("//table[@class= 'DataTable TrainList']");
		// travelling to row
		List<WebElement> row = table.findElements(By.tagName("tr"));
		System.out.println(row.size());
		for (int i = 0; i < row.size(); i++) 
		{
			
			List<WebElement> col = row.get(i).findElements(By.tagName("td"));
			String text = col.get(1).getText();
			System.out.println(text);
		}
	
		//travelling to column
		
		/*List<WebElement> column = row.get(2).findElements(By.tagName("td"));
		for (WebElement eachcol : column) 
		{
			
			System.out.println(eachcol.getText());
		
		
		}*/
		
		
		

	}

}
