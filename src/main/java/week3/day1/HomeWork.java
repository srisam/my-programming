package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class HomeWork {

	public static void main(String[] args) 
	
	{
	
		//invoking the chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//maximize the output window
		driver.manage().window().maximize();
		//load the url using get() method
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		

	}

}
