package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindEle 
{

	public static void main(String[] args) throws InterruptedException 
	
	{
	
		//invoking the chrome browser
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
				//maximize the output window
				driver.manage().window().maximize();
				//load the url using get() method
				driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
				List<WebElement> allLinks = driver.findElementsByTagName("a");
				int size = allLinks.size();
				System.out.println(size);
				Thread.sleep(2000);
				allLinks.get(6).click();
				
	}

}
