package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) 

	{

		//invoking the chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//maximize the output window
		driver.manage().window().maximize();
		//load the url using get() method
		driver.get("http://leaftaps.com/opentaps/control/main");
		//enter the username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//enter the password
		driver.findElementById("password").sendKeys("crmsfa");
		//click login
		driver.findElementByClassName("decorativeSubmit").click();
		//click the crmsfa link
		driver.findElementByLinkText("CRM/SFA").click();
		//left corner click the create lead link
		driver.findElementByLinkText("Create Lead").click();
		//enter company name
		driver.findElementById("createLeadForm_companyName").sendKeys("HCL");
		//enter firstname
		driver.findElementById("createLeadForm_firstName").sendKeys("srivamsi");
		//enter lastname
		driver.findElementById("createLeadForm_lastName").sendKeys("testleaf");
		//accessing source option using select method
		WebElement source=driver.findElementById("createLeadForm_dataSourceId");
		Select dd=new Select(source);
		dd.selectByVisibleText("Direct Mail");
		WebElement src=driver.findElementById("createLeadForm_marketingCampaignId");
		Select pd=new Select(src);
		pd.selectByValue("CATRQ_CARNDRIVER");
		WebElement ind=driver.findElementById("createLeadForm_industryEnumId");
		Select er=new Select(ind);
		List<WebElement>  options=	 er.getOptions();
		for (WebElement each : options) {
			String text = each.getText();
			if (text.startsWith("M"))
			{
			
				System.out.println(each.getText());
			
			}
		}
		
	
		
		//finally click the create lead
		//driver.findElementByClassName("smallSubmit").click();





	}

}
