package week2.day2;

import java.util.Scanner;

public class PrimaRange {
	
	public static void main(String[] args) {
	
		Scanner s= new Scanner(System.in);
		System.out.println("Enter the rang to find the prime number");
		int r=s.nextInt();
		for (int i=2;i<=r;i++)
		{
			int count=0;
			for (int j=1;j<=i;j++)
			{
				if(i%j==0)
				{
					count++;
				}
			}
			
			if(count==2)
			{
				System.out.print(i +"is a prime number ");
			}
		}
	}

}
