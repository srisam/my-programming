package week2.day2;

import java.util.Map;
import java.util.TreeMap;

public class MapPrac {

	public static void main(String[] args) 
	{

		String company ="Amazon India Private Limited";
		char[] ch = company.toCharArray();
		for (char c : ch) {
			System.out.println(c);
		}
		int value=0;
		Map<Character,Integer> map =new TreeMap<>();
		for (char d : ch) {
			if (map.containsKey(d)) {
				value = map.get(d)+1;
				map.put(d, value);
			} else {
				map.put(d, 1);
			}
		}
		System.out.println(map);
	}


}

