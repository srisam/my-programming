package wdMethods;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class LearnReport 
{
	@Test
	public void name() throws IOException
	{
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		ExtentTest logger = extent.createTest("CreateLea", "Create a new lead");
		logger.assignAuthor("Srivatsan");
		logger.assignCategory("smoke");
		
		logger.log(Status.PASS, "the demo sales manager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		
		logger.log(Status.PASS, "the crmsfa entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		
		logger.log(Status.PASS, "the button click is successful", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		
		extent.flush();
		
		
		
	}

}
