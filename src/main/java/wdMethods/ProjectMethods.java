package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods
{

	@BeforeSuite(groups="any")
	public void beforeSuite()
	{
		System.out.println("the beforesuite");
	}
	
	@BeforeTest(groups="any")
	
		public void beforeTest()
		{
			System.out.println("the beforeTest");
		}
	@BeforeClass(groups="any")
	public void beforeClass()
	{
		System.out.println("the before class");
	}
	
	@Parameters({"url","uname","pwd"})
	@BeforeMethod(groups="any")
	public void login(String url,String username, String password) 
	{
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
	   type(eleUsername, username);
	   WebElement elePassword = locateElement("id","password");
	   type(elePassword, password);
	   WebElement eleLogin = locateElement("class","decorativeSubmit");
	   click(eleLogin);
	   WebElement eleCrm = locateElement("link", "CRM/SFA");
	   click(eleCrm);
		
	}
	@AfterMethod(groups="any")
	
	public void afterMethod()
	{
		System.out.println("the aftermethod ");
	}
	
	@AfterClass(groups="any")
	public void afterClass()
	{
		System.out.println("the afterclass");
	}
	@AfterSuite(groups="any")
	public void afterSuite()
	{
		System.out.println("the aftersuite");
	}
	
	public void close() 
	{
	
		closeBrowser();
	}
	
	
}
