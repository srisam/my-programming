package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertFramePrac 
{
	public static void main(String[] args) 
	{
		//invoking the chrome browser
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
				//maximize the output window
				driver.manage().window().maximize();
				//load the url using get() method
				driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
				//switch to frame first, to identify the frame click in page and check via frame resourec is there
				 //and then right click inspect and find frames using <frame or <iframe tag
				//nd then ctrl+f find the particular frame and not down the locators
				driver.switchTo().frame("iframeResult");
				//right click try button -->inspect->locators->or xpath->actions(click)
				driver.findElementByXPath("//button[text()='Try it']").click();
				//switch to alert enter the text using sendkeys
				driver.switchTo().alert().sendKeys("Rock");
				//and then click ok using alert method accept
				driver.switchTo().alert().accept();
				//Verifying the text using get()method and storing in variable using 'String text'
				String text = driver.findElementById("demo").getText();
				//Checking the string using contains method
				if(text.contains("Rock"))
				{
					System.out.println("matched");
				}
				else
				{
					System.out.println("not matched");
				}
				
	}		
		
	}


