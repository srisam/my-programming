package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetWindowHandlesPrac 
{

	public static void main(String[] args) throws IOException 
	{
		//invoking the chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//maximize the output window
		driver.manage().window().maximize();
		//load the url using get() method
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByLinkText("Contact Us").click();
		System.out.println(driver.getTitle());
		//Interact with second window using getwindowHandles() method,tp get collections of another window use store geywindowshandles in variable called set,
		//set is nothing but collections
		Set<String> allWindows = driver.getWindowHandles();
		//now we have to convert set into list beacuse we are not able to access index in set
		//first create object for list interface
		List<String> lst=new ArrayList<>();
		//to add the list using add()method
		//converting set to list
		lst.addAll(allWindows);
		//now switching to second window
		driver.switchTo().window(lst.get(1));
		//to get title of the window using getTitle()window
		System.out.println("The title of the second window is " +driver.getTitle());
		//to get snapshot of the current page using getscreenshotas() method
		//and store it in a variable
		File src = driver.getScreenshotAs(OutputType.FILE);
		//create object
		File obj=new File("./Irctc/Irctcc.jpeg");
		//For interacting with window third party approaching called apache
		FileUtils.copyFile(src, obj);
		//to close the parent window 
		driver.switchTo().window(lst.get(0));
		driver.close();
		
		
		
		
		

	}

}
