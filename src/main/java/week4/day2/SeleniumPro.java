package week4.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumPro 

{

	public static void main(String[] args)
	{
		
		//invoking the chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//maximize the output window
		driver.manage().window().maximize();
		//load the url using get() method
		driver.get("https://www.flipkart.com/");
		driver.findElementByXPath("//button[@class=\"_2AkmmA _29YdH8\"]").click();
		driver.findElementByClassName("LM6RPg").sendKeys("iphone x");
		driver.findElementByXPath("//button[@type='submit']").click();
		driver.findElementByXPath("//a[text()='Apple iPhone X (Silver, 256 GB)']");
	}
}

